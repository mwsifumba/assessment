# HearX assessment #


### Prerequisites ###

* Git
* Docker
* Go

### How do I get set up? ###

> It's assumed your go `$PATH` is set to `$HOME/go`

* [Clone the repository](https://bitbucket.org/mwsifumba/assessment/src/master/)
`git clone git@bitbucket.org:mwsifumba/assessment.git`
* Edit the .env and provide all the values:
  - `DB_USER=` (postgres)
  - `DB_HOST=` (**docker.for.mac.localhost** for MacOs and *localhost* for linux)
  - `DB_PWD=` (password123)
  - `APP_PORT=` (8080)
  - `FROM_EMAIL=` (mailgun@sandbox7ef4db05cbdc4f689804fb58e372befb.mailgun.org)
  - `API_KEY=` (bc4201c469fb3929fd14d52670887e2d-f7d0b107-d6580091)
  - `API_URL=` (https://api.mailgun.net/v3/sandbox7ef4db05cbdc4f689804fb58e372befb.mailgun.org/messages)
* Edit the `database.env` file and provide all values:
  - `POSTGRES_USER=` (postgres)
  - `POSTGRES_PASSWORD=` (password123)

* Run: `docker-compose up --build`

---
### Interacting with the app's API ###

The base URL will be: `http://localhost`

| Endpoint        | Content Type           | Params  |
| :-------------: |:-------------:| -----:|
| `/email`      | `application/json` | ```{'subject': 'The email subject', 'body': 'The emaiil content', 'email': 'The email address where the email will be sent'}``` |

The below table lists responses returned by the API

| Response        |            | 
| :-------------: |:-------------| 
| `200`      | **The request was processed successfully**  |
| `400`      | **The request body has missing data** |
| `401`      | **The request could not be processed due to authorization. In this case please ensure your authentication token is valid**

> Requests require an OAuth 2.0 Bearer Access Token, please use this one: `eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IjJQdGJ4MGstNHNJdDhTUW85aVZIQjVON0YtNCIsImtpZCI6IjJQdGJ4MGstNHNJdDhTUW85aVZIQjVON0YtNCJ9`

##Misc
#####Improvements
* Separate the `RUN` and `TEST` commands for proper implementation
* Investigate why the database object has to be instantiated in both main.go and test_main.go (find a way to reuse the object)
* Customise database server port

#####Struggles
* Getting the containers to be multipart was a bit of a challenge given that I hadn't done it before so as work-around I ran commands and made the second one dependent on the first. So if unit tests succeed, then the application runs.
* I had issues registering on sendgrid, the site says I should contact support. I went with mailgun as an alternative.

