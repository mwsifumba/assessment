package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	_ "github.com/lib/pq"
)

// EmailLog | To be used for mapping email logs which will be store in the DB
type EmailLog struct {
	ID          int
	ToAddress   string
	FromAddress string
	Subject     string
	Body        string
	CreatedAt   time.Time
}

// AuthToken | To be used for mapping auth tokens when being retrieved from the DB
type AuthToken struct {
	ID        int
	Token     string
	CreatedAt time.Time
	UpdatedAt time.Time
}

// SampleRequest | To be used for mapping HTTP requests to the /email endpoin
type SampleRequest struct {
	Email   string
	From    string
	Subject string
	Body    string
}

var client = &http.Client{}
var dbConn *sql.DB
var apiKey = flag.String("API_KEY", "", "")
var dbHost = flag.String("DB_HOST", "", "")
var dbUser = flag.String("DB_USER", "", "")
var dbPwd = flag.String("DB_PWD", "", "")
var fromEmail = flag.String("FROM_EMAIL", "", "")
var apiURL = flag.String("API_URL", "", "")
var applicationPort = flag.String("APP_PORT", "", "")

// Testing flags
var testTo = flag.String("TO_EMAIL", "", "")
var testBody = flag.String("EMAIL_BODY", "", "")
var testAccessToken = flag.String("ACCESS_TOKEN", "", "")
var testSubject = flag.String("EMAIL_SUBJEC", "", "")

func logEmail(toAddr string, fromAddr string, subject string, body string, code int) {

	sqlStmt := `INSERT INTO email_logs (to_address, from_address, subject, body, status_code) VALUES ($1, $2, $3, $4, $5)`
	_, err := dbConn.Exec(sqlStmt, toAddr, fromAddr, subject, body, code)
	if err != nil {
		log.Fatal("Error while saving email log: ", err)
	}
}

func authorize(token string) bool {

	var authTkn AuthToken
	sqlStmt := `SELECT token FROM auth_tokens WHERE token = $1;`
	err := dbConn.QueryRow(sqlStmt, token).Scan(&authTkn.Token)

	if err != nil {
		log.Fatal("Error while validating token: ", err)
		return false
	}

	// if authTkn.Token == "" {
	// 	return false
	// }

	return true
}

func sendEmail(to string, subject string, body string) int {

	reqForm := url.Values{
		`from`:    {*fromEmail},
		`to`:      {to},
		`subject`: {subject},
		`text`:    {body},
	}

	req, err := http.NewRequest(`POST`, *apiURL, strings.NewReader(reqForm.Encode()))
	if err != nil {
		log.Fatal("Error while creating email request object: ", err)
		return -1
	}

	req.SetBasicAuth(`api`, *apiKey)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded;")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(`Error while sending email: `, err)
	}

	return resp.StatusCode
}

func processEmail(w http.ResponseWriter, req *http.Request) {

	// Prevent further proccessing if the request method is not POST
	if req.Method != "POST" {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	// Get the auth token from the request header and check if it's valid.
	token := req.Header.Get("Authorization")
	isAuthed := authorize(token)
	// Prevent further proccessing if the token is not valid.
	if !isAuthed {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	decoder := json.NewDecoder(req.Body)

	// Extract user data which came through the request body.
	var sr SampleRequest
	err := decoder.Decode(&sr)
	// Should there be an error while extracting user data, prevent further proccessing and log the error.
	if err != nil {
		fmt.Println("Error while decoding user request: ", err)
		return
	}

	// If any of the required fields is not populated, respond with the error message and prevent further proccessing.
	if sr.Email == "" || sr.Body == "" || sr.Subject == "" {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "The subject, body and email fields must all be populated")
		return
	}
	// Send the email.
	responseCode := sendEmail(sr.Email, sr.Subject, sr.Body)
	// Log the email and it's response code.
	logEmail(sr.Email, *fromEmail, sr.Subject, sr.Body, responseCode)
}

func main() {

	flag.Parse()

	connStr := fmt.Sprintf(`host=%s user=%s password=%s sslmode=disable`, *dbHost, *dbUser, *dbPwd)

	var err error

	// Open database connection
	dbConn, err = sql.Open("postgres", connStr)
	// Abort if he application cannot connect to the database.
	if err != nil {
		log.Fatal("Error while connecting to database")
		panic(err)
	}
	defer dbConn.Close()

	http.HandleFunc("/email", processEmail)

	port := fmt.Sprintf(`:%s`, *applicationPort)
	fmt.Println("Server running on port: " + port)

	http.ListenAndServe(port, nil)
}
