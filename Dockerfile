FROM postgres:12.3-alpine as database
WORKDIR /go/src/assessment
EXPOSE 5432

FROM golang:latest as go
EXPOSE 8080