package main

import (
	"database/sql"
	"fmt"
	"log"
	"testing"
)

func TestSendMail(t *testing.T) {
	connStr := fmt.Sprintf(`host=%s user=%s password=%s sslmode=disable`, *dbHost, *dbUser, *dbPwd)

	var err error

	dbConn, err = sql.Open("postgres", connStr)

	if err != nil {
		log.Fatal("Error while connecting to database")
		panic(err)
	}
	defer dbConn.Close()

	tokenOk := authorize("Bearer " + *testAccessToken)
	if !tokenOk {
		t.Errorf("Access token is not valid")
	}

	status := sendEmail(*testTo, *testSubject, *testBody)
	if status != 200 {
		t.Errorf("Send Email test failed")
	}

	logEmail(*testTo, *fromEmail, *testSubject, *testBody, status)
}
